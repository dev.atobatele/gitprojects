#!bin/bash

touch ~/empty/tim_$FILES.txt

FILES=$(ls ~/empty | wc -l)

until (($FILES % 25 == 0))
do

((FILES++))
FILES=$(ls ~/empty | wc -l)
touch ~/empty/tim_$FILES.txt
echo $FILES

done
